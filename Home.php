<?php

namespace App\Controllers;

use App\Models\UserModel;

use App\Models\FetchModel;
use App\Models\TypoDossierModel;

use App\Models\BrunchersModel;
use App\Models\GbonhiModel;

class Home extends BaseController
{
	public function index()
	{
		$data = [];
		$data['chageetude'] = 'active';

		/* if (session()->get('FirstName')) {
			$data['islogin'] = 'active';
		} */
		$this->FetchModel = new FetchModel();

		

		echo view('template/header', $data);
		echo view('choisirtpe');
		//echo view('newuser', $datainfo);
		//echo view('dash', $datainfo);
		echo view('template/footer');
	}
	public function singleparticipation()
	{

		$data = [];
		$data['chageetude'] = 'active';

		$this->FetchModel = new FetchModel();

		

		echo view('template/header', $data);
		//echo view('choisirtpe', $datainfo);
		echo view('newuser');
		//echo view('dash', $datainfo);
		echo view('template/footer');
	}
	public function saveuser()
	{
		helper(['form']);
		$rules = [

			'gender'          => 'required|min_length[2]|max_length[50]',
			'lastName'          => 'required|min_length[2]|max_length[50]',
			'firstName'          => 'required|min_length[1]|max_length[50]',
			'phonenumber'          => 'required|min_length[1]|max_length[50]',
			'email'          => 'required|min_length[1]|max_length[50]',
			//'terms'          => 'required|min_length[2]|max_length[50]',

		];
		if ($this->validate($rules)) {
			$verifuser['email'] =  $this->request->getPost('email');
			$verifphonenumberuser['phonenumber'] =  $this->request->getPost('phonenumber');
			$emailparticipant =  $this->request->getPost('email');
			$this->FetchModel = new FetchModel();
			$data['enregi'] = $this->FetchModel->verify_participant_beforeInsert($verifuser);
			$data['phoneenregi'] = $this->FetchModel->verify_phoneparticipant_beforeInsert($verifphonenumberuser);
			if ($data['enregi']) {
				$data['dejainscrit'] = "Cet Email est déja inscrit pour le  B Martini";

                    echo view('template/header', $data);
                    echo view('newuser', $data);
                    echo view('template/footer');
			}elseif($data['phoneenregi'] ){
				$data['dejainscrit'] = "Ce numéro est déja inscrit pour le B Martini";

				echo view('template/header', $data);
				echo view('newuser', $data);
				echo view('template/footer');
			}else {
				$LastName=$this->request->getPost('lastName');
				$gender=$this->request->getPost('gender');
				$FirstName=$this->request->getPost('firstName');
				$PhoneNumber=$this->request->getPost('phonenumber');
				$email=$this->request->getPost('email');
				$codemartini = 'PARTICIPANT_';
				$model = new BrunchersModel();
				$newUserData = [
					'gender' => $this->request->getPost('gender'),
					'LastName' => $this->request->getPost('lastName'),
					'FirstName' => $this->request->getPost('firstName'),
					'commune' => $this->request->getPost('commune'),
					'PhoneNumber' => $this->request->getPost('phonenumber'),
					'email' => $this->request->getPost('email'),
					'idgbonhi' => '',
					'namegbonhi' => '',
					'nbrmembre' => '',
					'idevent' => 1,
					'nameevent' => 'BMartini 5',
					'codemartini' => $codemartini,
					'createdAt' => date("Y-m-d H:i:s"),
					'updatedAt' => '',
				];
				$model->save($newUserData);
	            $idenregistrement = $model->insertID();
	            $newcode='PARTICIPANT_'.$idenregistrement;
				$to = $emailparticipant . ',bmartiniciv@gmail.com';
				$subject = 'Réservation reçue';
				$message = 'Bonjour ' . ' ' . $gender . ' '.$LastName. ' ' . $FirstName .'.'. "\n" . 'Nous avons bien reçu votre Réservation pour le B Martini du 6 Novembre. ' . ' '. "\n" . 'Votre code B Martini est : ' . $newcode .'.'. "\n" . 'Merci à bientôt! ' ;
				$email = \Config\Services::email();


				$email->setTo($to);
				$email->setFrom('bmartiniciv@gmail.com', 'B Martini: Réservation reçue');

				$email->setSubject($subject);
				$email->setMessage($message);

				if ($email->send()) {
					$curencydata['curencyuserid'] = $model->insertID();
					$curencyinfo['participantinfo'] = $this->FetchModel->selectparticipant($curencydata);
					$data['greffier'] = 'active';
					echo view('template/header', $data);
					echo view('single_summary', $curencyinfo);
					echo view('template/footer');
				} else {
					//print_r('Erreru');
					echo 'Erreru';
				}
			}
		} else {
			$datamessage['validation'] = $this->validator;
			
			$data['archiviste'] = 'active';
			echo view('template/header', $data);
			echo view('newuser', $datamessage);
			echo view('template/footer');
		}
	}


	public function definirquantite()
	{

		$data['archiviste'] = 'active';
		$this->FetchModel = new FetchModel();

		

		echo view('template/header', $data);
		echo view('quantite');
		//echo view('newuser', $datainfo);
		//echo view('dash', $datainfo);
		echo view('template/footer');
	}
	public function creategbonhi()
	{

		$data['archiviste'] = 'active';

		$rules = [

			'quantite'          => 'required|min_length[1]|max_length[50]',
		];
		if ($this->validate($rules)) {
			$this->FetchModel = new FetchModel();
			$datainfo['nbgbonhi'] =  $this->request->getPost('quantite');

			

			echo view('template/header', $data);
			echo view('gbonhi', $datainfo);
			//echo view('newuser', $datainfo);
			//echo view('dash', $datainfo);
			echo view('template/footer');
		} else {

			$datamessage['validation'] = $this->validator;
			$this->FetchModel = new FetchModel();

			

			echo view('template/header', $data);
			echo view('quantite');
			//echo view('newuser', $datainfo);
			//echo view('dash', $datainfo);
			echo view('template/footer');
		}
	}


public function savegbonhi()
{

		$data['islogin'] = 'active';
		$data['tracker'] = 'nav-active';

		$rules = [

			'eventname'          => 'required|min_length[1]|max_length[50]',
		];
		if ($this->validate($rules)) {
			//$getprojetdata1 =  $this->request->getPost('IDproject');
			// print_r($getprojetdata1);	

            $datainfo['nbgbonhi'] =  $this->request->getPost('quantite');
			$phonenumber =  $this->request->getPost('phonenumber');

			//$verifuser['email'] =  $this->request->getPost('email');

			//print_r($getdataarry);
			$erreurmail=0;
			$erreurnumber=0;
			foreach ($phonenumber  as $key => $phonenumbervalue) {
				$verifphonenumberuser['phonenumber'] =  $phonenumbervalue;
				$emailarray =  $this->request->getPost('email');
				$emailparticipant =  $emailarray[$key];
				$verifuser['email'] =  $emailparticipant;
				$this->FetchModel = new FetchModel();
				$data['enregi'] = $this->FetchModel->verify_participant_beforeInsert($verifuser);
				$data['phoneenregi'] = $this->FetchModel->verify_phoneparticipant_beforeInsert($verifphonenumberuser);

				if ($data['enregi']) {
				    $erreurmail= $erreurmail+1;
					$datainfo['dejainscrit'] = 'L email '.$emailparticipant. ' est déja inscrit pour le  B Martini';
                    
					
				}
				if ($data['phoneenregi']) {
				    $erreurnumber= $erreurnumber+1;
					$datainfo['dejainscrit'] = 'Le numéro '.$phonenumbervalue. ' est déja inscrit pour le B Martini';

					
				}
			}
			if($erreurmail>0 || $erreurnumber>0){
			    echo view('template/header', $data);
					echo view('gbonhi', $datainfo);
					echo view('template/footer');
			}else{
			    
		
			$codegbonhi = 'Gbonhi-' . rand(999999, 111111);
			$model = new GbonhiModel();
			$newUserData = [
				'nomgbonhi' => $this->request->getPost('nomgbonhi'),
				'codegbonhi' => $codegbonhi,
				'nbrmembre' => $this->request->getPost('quantite'),
				'idevent' => '1',
				'nameevent' => 'BMartini 6',
				'createdAt' => date("Y-m-d H:i:s"),
				'updatedAt' => '',
			];
			$model->save($newUserData);
			$curencydata['curencygbonhi'] = $model->insertID();
            $idgbonhi = $model->insertID();

            $prefgbonhi = $this->request->getPost('nomgbonhi');
            $createnomgbonhi= $prefgbonhi.''.$idgbonhi;
			$email =  $this->request->getPost('email');

			//$verifuser['email'] =  $this->request->getPost('email');

			//print_r($getdataarry);
			foreach ($email  as $key => $emailvalue) {

				$namegbonhi=$createnomgbonhi;
				$genderarray =  $this->request->getPost('gender');
				$gender =  $genderarray[$key];

				$lastNamearray =  $this->request->getPost('lastName');
				$lastName =  $lastNamearray[$key];

				$firstNamearray =  $this->request->getPost('firstName');
				$firstName =  $firstNamearray[$key];

				$communearray =  $this->request->getPost('commune');
				$commune =  $communearray[$key];
				$phonenumberarray =  $this->request->getPost('phonenumber');
				$phonenumber =  $phonenumberarray[$key];
				$codemartini = 'PARTICIPANT_';

				$BrunchersModel = new BrunchersModel();

				$data = [
					'gender' => $gender,
					'LastName' => $lastName,
					'FirstName' => $firstName,
					'commune' =>  $commune,
					'PhoneNumber' => $phonenumber,
					'email' => $emailvalue,
					'idgbonhi' => $curencydata['curencygbonhi'],
					'namegbonhi' => $this->request->getPost('nomgbonhi'),
					'nbrmembre' => $this->request->getPost('quantite'),
					'idevent' => 1,
					'nameevent' => 'B Martini 6',
					'codemartini' => $codemartini,
					'createdAt' => date("Y-m-d H:i:s"),
					'updatedAt' => '',

				];
				$BrunchersModel->save($data);
				 $idenregistrement = $model->insertID();
	            $newcode='PARTICIPANT_'.$idenregistrement;
				$to = $emailparticipant . ',bmartiniciv@gmail.com';
				$subject = 'Réservation reçue';
				$message = 'Bonjour ' . ' ' . $gender . ' ' . $lastName . ' ' . $firstName .'.'. "\n" . 'Nous avons bien reçu votre réservation pour le B Martini du 6 Novembre ' . ' ' . "\n" . 'Votre code B Martini est : ' . $newcode .'.'. "\n" . 'Nom du Gbonhi : ' . $namegbonhi .'.'. "\n" . 'Merci à bientôt!';
				//$message = 'Bonjour ' . ' ' . $gender . '  ' . '  ' . $lastName . '  ' . $firstName . "\n" . 'Nous avons bien reçu votre réservation  ' .' ' . "\n" . 'Votre code B Martini est : ' . $codemartini . "\n" . 'Merci à bientôt' ;

				$email = \Config\Services::email();


				$email->setTo($to);
				$email->setFrom('bmartiniciv@gmail.com', 'B Martini: Réservation reçue');

				$email->setSubject($subject);
				$email->setMessage($message);
				$email->send();
			}

			$data['succes'] = "Réservation éffectuée";
			$this->FetchModel = new FetchModel();

			$curencyinfo['participantinfo'] = $this->FetchModel->selectgbonhi($curencydata);
			$data['greffier'] = 'active';
			echo view('template/header', $data);
			echo view('gbonhi_summary', $curencyinfo);
			echo view('template/footer');
			}
		} else {
			$this->FetchModel = new FetchModel();
			echo view('template/header', $data);
			echo view('quantite');
			//echo view('newuser', $datainfo);
			//echo view('dash', $datainfo);
			echo view('template/footer');
		}
	}
}
